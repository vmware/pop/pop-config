"""
A simple pop program to test loggers
"""
import asyncio

import pop.hub

if __name__ == "__main__":
    hub = pop.hub.Hub()
    hub.pop.loop.create()
    hub.pop.sub.add(dyne_name="config")
    hub.config.integrate.load("pop_config", "pop_config")
    hub.log.trace("trace")

    if "timed" in hub.OPT.pop_config.log_plugin:
        # Allow timed rotating loggers to roll-over
        hub.pop.Loop.run_until_complete(asyncio.sleep(1))

    if "rotating" in hub.OPT.pop_config.log_plugin:
        # Overflow the logs so a backup gets made
        hub.log.trace("trace")

    if "async" in hub.OPT.pop_config.log_plugin:
        # Allow async loggers to have another free cycle to write
        hub.pop.Loop.run_until_complete(asyncio.sleep(0.1))
