import os
import pathlib
import subprocess
import sys
import tempfile

import pytest


@pytest.fixture(name="logfile", scope="function")
def logfile_(tmpdir):
    fh = tempfile.NamedTemporaryFile(suffix=".log", delete=False, dir=tmpdir)
    path = pathlib.Path(fh.name)
    yield path


def run_cli(*args, env=None, check: bool = True):
    if env is None:
        env = {}

    if os.name == "nt" and "SYSTEMROOT" not in env:
        env["SYSTEMROOT"] = os.getenv("SYSTEMROOT")

    runpy = pathlib.Path(__file__).parent / "run.py"
    command = [sys.executable, str(runpy), *args]

    proc = subprocess.Popen(
        command,
        encoding="utf-8",
        env=env,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )
    retcode = proc.wait()
    if check:
        assert retcode == 0, proc.stderr.read()

    # We only care about what was printed to the logs
    return proc.stderr.read() or proc.stdout.read()


@pytest.fixture(name="cli", scope="function")
def cli():
    return run_cli
