import re

import pytest

INVALID_PATTER = r"invalid choice: \\?'invalid\\?' \(choose from \\?'disabled\\?', \\?'enabled\\?', \\?'strict\\?'\)"


def test_default(cli):
    """
    Verify that a default option with a valid value succeeds with no error
    """
    OPT = cli(
        load=(["choices"], "choices"),
    )
    assert OPT.choices.picker == "enabled"


def test_cli_valid(cli):
    """
    Verify that a cli option with a valid value succeeds with no error
    """
    OPT = cli(
        f"--picker=disabled",
        load=(["choices"], "choices"),
    )
    assert OPT.choices.picker == "disabled"


def test_cli_list_valid(cli):
    """
    Verify that a cli option with a valid value succeeds with no error
    """
    OPT = cli(
        f"--list-picker",
        "1",
        "2",
        "3",
        load=(["choices"], "choices"),
    )
    assert OPT.choices.list_picker == ["1", "2", "3"]


def test_cli_list_invalid(cli):
    """
    Verify that a cli option with a valid value succeeds with no error
    """
    with pytest.raises(AssertionError) as e:
        cli(
            f"--list-picker",
            "1",
            "2",
            "3",
            "invalid",
            load=(["choices"], "choices"),
        )
    assert "invalid choice" in str(e.value)


def test_os_valid(cli):
    """
    Verify that an OS variable with a valid value succeeds with no error
    """
    OPT = cli(
        load=(["choices"], "choices"),
        env={"PICKER": "disabled"},
    )
    assert OPT.choices.picker == "disabled"


def test_config_valid(cli, cpath_dir):
    """
    Verify that a config option with a valid value succeeds with no error
    """
    OPT = cli(
        "--config",
        str(cpath_dir / "valid_choices.conf"),
        load=(["choices"], "choices"),
    )
    assert OPT.choices.list_picker == ["1", "2", "3"]


def test_config_list_valid(cli, cpath_dir):
    """
    Verify that a config option with a valid value succeeds with no error
    """
    OPT = cli(
        "--config",
        str(cpath_dir / "valid_choices.conf"),
        load=(["choices"], "choices"),
    )
    assert OPT.choices.picker == "disabled"


def test_config_list_invalid(cli, cpath_dir):
    """
    Verify that a config option with a valid value succeeds with no error
    """
    with pytest.raises(AssertionError) as e:
        cli(
            "--config",
            str(cpath_dir / "invalid_choices.conf"),
            load=(["choices"], "choices"),
        )
    assert "invalid choice" in str(e.value)


def test_cli_invalid(cli):
    """
    Verify that a cli option with an invalid value fails with an error
    """
    with pytest.raises(AssertionError) as e:
        cli(
            f"--picker=invalid",
            load=(["choices"], "choices"),
        )

    assert re.search(INVALID_PATTER, str(e.value))


def test_os_invalid(cli):
    """
    Verify that an OS variable with an invalid value fails with an error
    """
    with pytest.raises(AssertionError) as e:
        cli(
            load=(["choices"], "choices"),
            env={"PICKER": "invalid"},
        )

    assert re.search(INVALID_PATTER, str(e.value))


def test_config_invalid(cli, cpath_dir):
    """
    Verify that a config option with an invalid value fails with an error
    """
    with pytest.raises(AssertionError) as e:
        cli(
            "--config",
            str(cpath_dir / "invalid_choices.conf"),
            load=(["choices"], "choices"),
        )
    assert re.search(INVALID_PATTER, str(e.value))
