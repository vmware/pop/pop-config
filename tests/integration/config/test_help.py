import sys


def test_help(cli):
    ret = cli("--help", load=("pop_config", "pop_config"), check=False)
    assert "Logging Options" in ret
    assert "usage:" in ret
    if sys.version_info >= (3, 10):
        assert "options" in ret
    else:
        assert "optional arguments" in ret


def test_base_help_subcommand(cli):
    ret = cli("--help", load=("s1", "s1"), check=False)
    assert "Logging Options" in ret
    assert "usage:" in ret
    assert "positional arguments" in ret
    if sys.version_info >= (3, 10):
        assert "options" in ret
    else:
        assert "optional arguments" in ret


def test_help_subcommand(cli):
    ret = cli("--help", load=("s1", "s1"), check=False)

    assert "Logging Options" in ret
    assert "usage:" in ret

    if sys.version_info >= (3, 10):
        assert "options" in ret
    else:
        assert "optional arguments" in ret
