import os

import pytest


def test_file(cli, cpath_dir):
    OPT = cli(
        "--config",
        cpath_dir / "simple.conf",
        load=(["f1"], "f1"),
    )
    assert OPT.f1.test == "simple"
    assert OPT.f1.error is False


@pytest.mark.skipif(
    os.name == "nt",
    reason="This test doesn't run well on the shared windows vm in the pipeline",
)
def test_shebang(cli, cpath_dir):
    OPT = cli(
        "--config",
        str(cpath_dir / "pipe.conf"),
        load=(["f1"], "f1"),
    )
    assert OPT.f1.foo == "bar"


def test_file_default(cli, cpath_dir):
    OPT = cli(
        load=(["f3"], "f3"),
    )
    assert OPT.f3.cheese is True


def test_dir(cli, cpath_dir):
    OPT = cli(
        "--config-dir",
        str(cpath_dir / "simple.d"),
        load=(["f2"], "f2"),
    )
    assert OPT.f2.first is True
    assert OPT.f2.second is True
    assert OPT.f2.rocks is True
    assert OPT.f2.seeds is True


def test_includes(cli, cpath_dir):
    OPT = cli(
        "--config",
        str(cpath_dir / "include1.conf"),
        load=(["f1"], "f1"),
    )
    assert OPT.f1.included is True
    assert OPT.f1.test is True
