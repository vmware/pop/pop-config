=============
Ensure Config
=============

If your project is an app-merge component and uses hub.OPT, it may not be
guaranteed that the project merging yours added it explicitly to it's config.

All you have to do is extend the ``hub.config.LOAD`` list with your project
to ensure that it always is available on hub.OPT.

.. code-block:: python

    hub.pop.sub.add(dyne_name="config")
    hub.config.LOAD.append("my_project")
