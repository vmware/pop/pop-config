===========
App Merging
===========

Source
======

By default the *CLI_CONFIG* references the local *CONFIG* setting. The ``source``
option allows you to reference a documented configuration from a separate
project configuration. This powerful option allows you to manage the arguments
and flags in a namespace of an app that is being merged into this app. The
benefit here is that the *CONFIG* values do not need to be rewritten and you
maintain a single authoritative source of documentation.

When using ``source`` in the *CLI_CONFIG* the namespace that defined the option
in the *CONFIG* dictionary will own the option. This makes it easy to have
an application that uses its own config namespace be app merged into another
application that can then transparently manage the configuration of the
merged app.

Therefore, if we have 2 projects' conf.py files, one can reference the other.
The source option references the project name. So if the first file is in
project "test" and the second file is for project "other", an argument
can reference the conf.py in project "other":

``test``
.. code-block:: python

    CLI_CONFIG = {"test": {}, "oranges": {"source": "other"}}
    CONFIG = {"test": {"default": "Red", "help": "What color to test"}}


``other``

.. code-block:: python

    CLI_CONFIG = {}

    CONFIG = {
        "oranges": {
            "default": "Many",
            "help": "The amount of oranges to enjoy.",
        },
    }

Dyne
====

A powerful option in the *CLI_CONFIG* is ``dyne``. This uses vertical app
merging to modify another application's cli options. This allows a vertical
app merge repo to define cli arguments that will be made available when the
plugins are installed to extend an external app.
